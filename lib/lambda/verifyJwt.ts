import { verify } from 'jsonwebtoken';

import { Session } from '../interfaces/Session';

export function verifyJwt(token?: string): Session | Error {
  if (!token) {
    return new Error('You do not appear to be logged in.');
  }

  const jwtSecret = process.env.jwt_secret;
  if (!jwtSecret) {
    // tslint:disable-next-line:no-console
    console.log('Deployment error: no JSON Web Token secret was defined in $TF_VAR_jwt_secret');

    return new Error('We are currently experiencing issues, please try again later.');
  }

  let session: Session;
  try {
    session = verify(token, jwtSecret, { algorithms: [ 'HS256' ] }) as Session;
  } catch (e) {
    return new Error('You do not appear to be logged in.');
  }

  return session;
}
