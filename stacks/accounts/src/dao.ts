import { Session } from '../../../lib/interfaces/Session';
import { Database } from '../../../lib/lambda/middleware/withDatabase';

export function insertSessionUuid(database: Database, uuid: string, refreshToken: string) {
  return database.none(
    // tslint:disable-next-line:no-invalid-template-strings
    'INSERT INTO sessions(identifier, refresh_token) VALUES (${identifier}, ${refreshToken})',
    { identifier: uuid, refreshToken },
  );
}

export async function getProfilesForAccounts(database: Database, accountIds: string[]) {
  interface Row {
    identifier: string;
    orcid: string;
    name?: string;
  }

  const result = await database.manyOrNone<Row>(
    'SELECT'
    + ' account.identifier'
    + ', orcid.orcid, orcid.name'
    + ' FROM accounts account'
    + ' JOIN orcids orcid ON account.orcid=orcid.orcid'
    + ' WHERE account.identifier IN ($1:csv)',
    [
      accountIds,
    ],
  );

  return result;
}

export async function getSessionForToken(database: Database, refreshToken: string): Promise<Session> {
  const result: { session_id: string; account_id?: string, orcid?: string } = await database.one(
    'SELECT s.identifier AS session_id, a.identifier AS account_id, a.orcid FROM sessions s'
    + ' LEFT JOIN accounts a ON s.account=a.identifier'
    // tslint:disable-next-line:no-invalid-template-strings
    + ' WHERE s.refresh_token=${refreshToken}',
    { refreshToken },
  );

  const account = (result.account_id)
    ? { identifier: result.account_id, orcid: result.orcid }
    : undefined;

  return {
    account,
    identifier: result.session_id,
  };
}

export function getAccountForOrcid(database: Database, orcid: string) {
  return database.oneOrNone(
    'SELECT identifier FROM accounts WHERE orcid=$1',
    [ orcid ],
  ) as Promise<null | { identifier: string }>;
}

export function storeOrcidCredentials(
  database: Database,
  orcid: string,
  accessToken: string,
  refreshToken: string,
  sessionId: string,
  accountId: string,
  name?: string,
) {
  return database.tx((t) => {
    return t.batch([
      t.none(
        'INSERT INTO orcids(orcid, access_token, refresh_token, name)'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' VALUES(${orcid}, ${accessToken}, ${refreshToken}, ${name}) ON CONFLICT (orcid) DO UPDATE'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' SET access_token=${accessToken}, refresh_token=${refreshToken}, name=${name}',
        { orcid, accessToken, refreshToken, name },
      ),
      t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'INSERT INTO accounts(identifier, orcid) VALUES (${identifier}, ${orcid}) ON CONFLICT (identifier) DO UPDATE'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' SET orcid=${orcid}',
        { identifier: accountId, orcid },
      ),
      t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'UPDATE sessions SET account=${accountId} WHERE identifier=${sessionId} AND account IS NULL',
        { sessionId, accountId },
      ),
    ]);
  }) as Promise<void>;
}
