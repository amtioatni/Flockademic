import { NewScholarlyArticle } from '../../../../lib/interfaces/endpoints/periodical';
import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function initialiseScholarlyArticle(
  database: Database,
  session: Session,
  scholarlyArticle: NewScholarlyArticle,
  periodicalIdentifier?: string,
  sameAs?: string,
  fullText?: { contentUrl: string; name: string; license?: string; },
): Promise<void> {
  return database.tx((t) => {
    const queries = [
      t.none(
        'INSERT INTO scholarly_articles (identifier, name, description, creator_session)'
          // tslint:disable-next-line:no-invalid-template-strings
          + ' VALUES (${identifier}, ${name}, ${description}, ${creator_session})',
        {
          ...scholarlyArticle,
          creator_session: session.identifier,
        },
      ),
    ];

    if (periodicalIdentifier) {
      queries.push(t.none(
        'INSERT INTO scholarly_articles_part_of (scholarly_article, is_part_of)'
          // tslint:disable-next-line:no-invalid-template-strings
          + ' SELECT ${identifier}, uuid FROM periodicals WHERE identifier=${periodicalIdentifier}',
        { identifier: scholarlyArticle.identifier, periodicalIdentifier },
      ));
    }

    if (sameAs) {
      queries.push(t.none(
        'INSERT INTO scholarly_articles_same_as (scholarly_article, same_as)'
          // tslint:disable-next-line:no-invalid-template-strings
          + ' VALUES (${identifier}, ${sameAs})',
        { identifier: scholarlyArticle.identifier, sameAs },
      ));
    }

    if (session.account) {
      queries.push(t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'INSERT INTO scholarly_article_authors (scholarly_article, author) VALUES (${scholarlyArticle}, ${author})',
        { scholarlyArticle: scholarlyArticle.identifier, author: session.account.identifier },
      ));

      scholarlyArticle.author = [ { identifier: session.account.identifier } ];
    }

    if (fullText) {
      queries.push(t.none(
        'INSERT INTO scholarly_article_associated_media (scholarly_article, content_url, name, license)'
          // tslint:disable-next-line:no-invalid-template-strings
          + 'VALUES (${scholarlyArticle}, ${contentUrl}, ${fileName}, ${license})',
        {
          contentUrl: fullText.contentUrl,
          fileName: fullText.name,
          license: fullText.license,
          scholarlyArticle: scholarlyArticle.identifier,
        },
      ));

      scholarlyArticle.associatedMedia = [ fullText ];
    }

    return t.batch(queries);
  });

}
