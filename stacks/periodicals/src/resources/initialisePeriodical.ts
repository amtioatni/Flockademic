import { v4 as uuid } from 'uuid';
import {
  NewPeriodical,
  PostCreatePeriodicalRequest,
  PostCreatePeriodicalResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { initialisePeriodical as service } from '../services/initialisePeriodical';

export async function initialisePeriodical(
  context: Request<PostCreatePeriodicalRequest> & DbContext & SessionContext,
): Promise<PostCreatePeriodicalResponse> {
  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }

  try {
    const proposedPeriodical = context.body.result || {};

    const newPeriodical: NewPeriodical = {
      description: proposedPeriodical.description,
      headline: proposedPeriodical.headline,
      identifier: uuid(),
      name: proposedPeriodical.name,
    };

    await service(context.database, context.session, newPeriodical);

    return { result: newPeriodical };
  } catch (e) {
    // tslint:disable-next-line:no-console
    console.log('Database error:', e);

    throw new Error('There was a problem creating a new journal, please try again.');
  }
}
